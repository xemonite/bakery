﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Bakery.Web.Startup))]
namespace Bakery.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
